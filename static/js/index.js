$(document).ready(function () {
 
    var base = 'http://127.0.0.1:8000/'
    var url = base + 'api/'

    var csrftoken = $('meta[name="csrf-token"]').attr('content')
    $('#example').DataTable()

    $.ajaxSetup({
        headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    // agency select
    $.get(url+'agency/', function (res){
        // agency result
        let result = res?.results

        // active agency
        var active_agency = result.filter(p => p.Active == true)
        // console.log(active_agency)
        $('#agencyid').attr('agency-id',active_agency)
        $('.heading .agency').html(active_agency[0]?.Agency_Name)
        $('#SelectCompany').append('<option value="'+active_agency[0]?.id+'">'+active_agency[0]?.Agency_Name+'</option>')
        // agency append
        for(var i=0;i<result.length;i++){
            if(result[i]?.Active == false){
                $('#SelectCompany').append('<option value="'+result[i]?.id+'">'+result[i]?.Agency_Name+'</option>')
            }
        }

    })

    // get order list data model
    $('.viewmaterial').click(function () {
        var materialid = $(this).attr('data-attr')
        $.get(url + 'order-list/' + materialid, function (data) {
            $('#viewmaterial .modal-title').text(data?.ProductName)
            $('#viewmaterial #company_name #name').text(data?.Name)
            $('#viewmaterial #company_name #cname').text(data?.CompanyName)
            $('#viewmaterial #hsncode #hsn').text(data?.Hsn_code)
            $('#viewmaterial #price #op').text(data?.OfferPrice)
            $('#viewmaterial #price #q').text(data?.Quantity)
            $('#viewmaterial #price #p').text(data?.Price)
        })

    })

    // select company name
    $('#SelectCompany').change(function () {
        // store it in session
        var id = $(this).val()
        $.get(base + 'agency/' + id, function (res) {
            if (res.status == 200) {
                $('#agencyupdatestatus').text(res.message)
                setTimeout(() => {
                    location.reload()
                }, 1000)
            }
        })
    })



    // select vendors
    $('#selectVendor').change(function () {
        var vendorid = $(this).val()
        $.get(url + 'vendors/' + vendorid, function (data) {
            $('#vendorname').val(data?.Name)
            $('#vendorid').val(data?.id)
            $('#vendormobile').val(data?.Mobile)
            $('#vendoremail').val(data?.Email)
        })
    })

    $('#customercompanyname').change(function () {
        var id = $(this).val()
        $.get(url + 'customers/' + id, function (data) {
            $('#customername').val(data?.CustomerName)
            $('#customerid').val(data?.id)
            $('#customermobile').val(data?.Mobile)
            $('#customeremail').val(data?.Email)
            $('#customeraddress').val(data?.Address)
            $('#customerstate').val(data?.State)
            $('#customercity').val(data?.City)
            $('#customerzip').val(data?.Zip)
            $('#customercountry').val(data?.Country)
        })
    })

    $('#PatchMethod').change(function () {
        var key = $(this).attr('data-col')
        var url = $(this).attr('data-url')
        var id = $(this).attr('data-id')
        var value = $(this).val()
        PATCHMethod(key, value, url + '/' + id + '/')
    })

    var putvariable = document.getElementById('putMethod')
    putvariable.addEventListener('show.bs.modal', function (event) {
        // Button that triggered the modal
        var button = event.relatedTarget
        // Extract info from data-bs-* attributes
        var id = button.getAttribute('data-bs-whatever')
        var Purl = button.getAttribute('data-url')
        // If necessary, you could initiate an AJAX request here
        $.get(url + Purl + '/' + id, function (data) {
            if (Purl == 'products') {
                $('#productName').val(data.Product)
                $('#productPrice').val(data.Price)
                $('#productGST').val(data.GST)
                $('.productupdate .update').attr('data-id', data.id)
            }
        })
    })

    $('.productupdate .update').click(function () {
        var params = {
            "Product": $('#productName').val(),
            "Price": $('#productPrice').val(),
            "GST": $('#productGST').val()
        }
        producturl = $(this).attr('data-url')
        id = $(this).attr('data-id')
        var urls = url + producturl + '/' + id + '/'
        PATCHPARAMMethod(params, urls)
        location.reload()
    })

    function PATCHPARAMMethod(params, dynamicurl) {
        $.ajax({
            type: 'PATCH',
            url: dynamicurl,
            data: JSON.stringify(params),
            processData: false,
            contentType: "application/json",
            dataType: "json",
            headers: {
                'X-CSRFToken': csrftoken
            },
            success: function (response) {

            },
            error: function (response) {
                alert('Network issue Try again later. !!!')
            }
        });
    }


    // patch method
    function PATCHMethod(key, value, dynamicurl) {

        patch = { [key]: value };
        baseurl = url + dynamicurl
        $.ajax({
            type: 'PATCH',
            url: baseurl,
            data: JSON.stringify(patch),
            processData: false,
            contentType: "application/json",
            dataType: "json",
            headers: {
                'X-CSRFToken': csrftoken
            },
            success: function (response) {
                // console.log(response)
            },
            error: function (response) {
                console.log(response);
            }
        });

    }



    $('#addproduct').click(function () {
        $.ajax({
            type: "POST",
            url: url + 'products/',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "Vendor": $('#vendor').val(),
                "Product": $('#productname').val(),
                "Price": $('#productprice').val(),
                "GST": $('#productgst').val(),
                "Agency": $('#agency').val(),
                "User": $('#user-id').attr('data-id')
            }),
            headers: {
                'X-CSRFToken': csrftoken
            },
            success: function (response) {
                $('.alert').addClass('alert-success').removeClass('d-none alert-danger')
                $('.alert strong').text('Product added successfully. !!!')
                setTimeout(function () {
                    location.reload()
                }, 2000)
            },
            error: function (response) {

                $('.alert').removeClass('d-none').addClass('alert-danger')
                $('.alert strong').text('All Fields are required. !!!')
            }
        });
    })



    var deleteProduct = document.getElementById('deleteMethod')
    deleteProduct.addEventListener('show.bs.modal', function (event) {
        // Button that triggered the modal
        var button = event.relatedTarget
        // Extract info from data-bs-* attributes
        var id = button.getAttribute('data-bs-whatever')
        // If necessary, you could initiate an AJAX request here
        $('#deleteMethod .dlt').attr('data-id', id)
        DELETEMethod()
    })
    function DELETEMethod() {
        $('#deleteMethod .dlt').click(function () {
            var customurl = $(this).attr('data-url')
            var key = $(this).attr('data-col')
            var value = $(this).attr('data-id')
            var baseurl = url + customurl + '/' + value
            var params = { [key]: value };
            $.ajax({
                type: "DELETE",
                url: baseurl,
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(params),
                headers: {
                    'X-CSRFToken': csrftoken
                },
            });
            $('.row_' + value).remove()
            $('#deleteMethod').modal('toggle');
        })
    }



    // daily order info
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.daily-order-needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    if (form.checkValidity()) {
                        event.preventDefault()
                        var params = {
                            User: Number($('#userid').val()),
                            Order_recived_date: $('#orderreciveddate').val(),
                            Delivery_date: $('#orderdeliverydate').val(),
                            Billing_company_name: Number($('#agencyid').val()),
                            Sales_company_name: Number($('#customercompanyname').val()),
                            Purchase_transport_name: Number($('#selectVendor').val()),
                            Unloading_address: $('#drivercontactname').val(),
                            Unloading_state: $('#customerstate').val(),
                            Unloading_city: $('#customercity').val(),
                            Unloading_zip: $('#customerzip').val(),
                            Unloading_country: $('#customercountry').val(),
                            Driver_contact_name: $('#drivercontactname').val(),
                            Driver_contact_number: $('#drivercontactnumber').val(),
                            Site_contact_name: $('#sitecontactname').val(),
                            Site_contact_number: $('#sitecontactnumber').val(),
                            PUR_rate: $('#purrate').val(),
                            PO_number: $('#ponumber').val(),
                            Sales_rate: 100,
                            CDC_number: $('#cdcnumber').val(),
                            IDC_number: $('#idcnumber').val(),
                            Materials: getorderList.split(','),
                            'csrfmiddlewaretoken': csrftoken
                        }

                        $.ajax({
                            type: "POST",
                            url: url + "daily-orders/",
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify(params),
                            headers: {
                                'X-CSRFToken': csrftoken
                            },
                            success: function (response) {
                                localStorage.removeItem('order-items')
                                $('#message').html('<div class="alert alert-primary alert-dismissible fade show" role="alert"><strong>Daily Order Info</strong> Created successfully. !!!<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>')
                                setTimeout(function () {
                                    location.reload()
                                }, 3000)
                            },
                            error: function (response) {
                                console.log(response);
                            }
                        });
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })();



    // order list functionalities
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.order-list')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {

                form.addEventListener('submit', function (event) {

                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    if (form.checkValidity()) {
                        event.preventDefault()
                        var formData = $(this).serialize();
                        $('#orderslist').removeClass('d-none')
                        $.post(url + 'order-list/', formData, function (data) {
                            // console.log(data)
                            $('#orderslist').append('<tr><td>' + data.ProductName + '</td><td>' + data.Name + '<br><small>(' + data.CompanyName + ')</small></td><td><div><b>Price :</b>' + data.Price + '</div><div><b>Offer Price :</b><span>' + data.OfferPrice + '</span></div></td><td><a href="' + data.id + '" class="editorderlist text-warning"><span data-feather="edit"></span></a><a href="' + data.id + '" class="deleteorderlist text-danger"><span data-feather="trash-2"></span></a></td></tr>')
                            $('.order-list').trigger("reset");
                            localstorageset('order-items', data.id)
                        })
                    }

                    form.classList.add('was-validated')

                }, false)
            })
    })();




    // Form Validation for Login, Logout and Forgot Password 
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })();


    // Form validation for add vendor disabling form submissions if there are invalid fields
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.addvendor')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })();

    // delete products from orders
    function localstorageset(data, id) {
        var getData = localStorage.getItem(data)
        if (getData == null) {
            localStorage.setItem(data, id)
            // console.log('new order item')
        } else {
            var orderitem = getData.split(',')
            orderitem.push(id)
            var orders = orderitem.toString()
            localStorage.setItem(data, orders)
            // console.log('update item')
        }
    }

    $('#FinalOrderSubmit').click(function () {
        var data = localStorage.getItem('order-items')
        if (data != null) {
            items = data.split(',')
            userid = $('#userid').val()

            var params = {
                'Order': items,
                'User': userid,
                'csrfmiddlewaretoken': csrftoken
            }
            console.log(params)

            $.post(url + 'orders/', params, function (data) {
                console.log(data)
                localStorage.removeItem('order-items')
                location.reload()
            })

        } else {
            alert('add atleast one product.')
        }
    })
    // quotation

    $('#quotionTable').on('click', '.quatotiondeleteMethod', function (event) {
        event.preventDefault()
        quotionId = $(this).attr('data-bs-whatever')
        $.ajax({
            type: 'DELETE',
            url: 'http://127.0.0.1:8000/api/daily-orders/' + quotionId + '/',
            data: { 'csrfmiddlewaretoken': $('meta[name="csrf-token"]').attr('content') },
            headers: { 'X-CSRFtoken': $('meta[name="csrf-token"]').attr('content') },
            success: function (data, status) {
                console.log(data)
            }
        })

    })

})