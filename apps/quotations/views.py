from django.shortcuts import render


# Create your views here.
def Quotations(request):
    return render(request, 'quotation/quotation.html')


def AddQuotations(request):
    return render(request, 'quotation/add_quotation.html')