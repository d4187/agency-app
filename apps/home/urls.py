from django.urls import path
from apps.home import views

urlpatterns = [
    path('', views.home, name="home"),
    path('phonebook', views.phonebooks, name="phonebooks"),
    path('gst-detail', views.gst_details, name="gst_details"),
    path('attendance', views.attendance, name="attendance"),
    path('file-management', views.FileManagement, name="filemanagement"),
]