from django.http.response import HttpResponse
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.http import HttpResponse
from apps.vendors.models import Vendor, VendorStaffDetail
from apps.products.models import Product
from django.core.paginator import Paginator
from apps.agency.models import Agency
from apps.customers.models import Customer
from apps.home.models import FileManagement as multifiles,Attendance
from django.contrib.auth.models import User
import calendar
import datetime

# Create your views here.

def home(request):

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is None:
            context = {"error": "Invalid username or password."}
            return render(request, '_index.html', context)
        login(request, user) 
        return redirect('/')
    return render(request, '_index.html')


def phonebooks(request):

    active = Agency.objects.all().filter(Active=True)
    context = {
        'agency_obj' : Agency.objects.all(),
        'active_agency_obj' : active,
        'customer_obj' : Customer.objects.filter(),
    }

    return render(request, 'phonebook/phonebook.html', {'obj':context})


def gst_details(request):
    return render(request, 'gst/gst-detail.html')

def attendance(request):
    now = datetime.datetime.now()
    attendance = Attendance.objects.all()
    users = User.objects.filter(is_staff=True)
    daysinmonth = calendar.monthrange(now.year, now.month)[1]
    days = []
    i=0
    while i < daysinmonth:
        i+=1
        days.append(i)
    return render(request, 'attendance/attendance.html',{
        'days': days,
        'users':users,
        'attendance':attendance,
        })

def FileManagement(request):
    if request.FILES:
        for f in request.FILES.getlist('filemanage'):
            uploadFile = multifiles.objects.create(File=f)
    filedata = multifiles.objects.all().order_by('-timestamp')
    return render(request,'filemanagement/filemanagement.html',{'filedata':filedata})