from django.shortcuts import render

# Create your views here.

def NetPaymentPayable(request):
    return render(request,'net_pay/netpayable.html')

def NetPaymentRecivable(request):
    return render(request,'net_pay/netrecivable.html')