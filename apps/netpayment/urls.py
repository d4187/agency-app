from django.urls import path
from apps.netpayment import views

urlpatterns = [
        path('', views.NetPaymentPayable, name="netpay"),
        path('net-recivable', views.NetPaymentRecivable, name="netrecive"),
]