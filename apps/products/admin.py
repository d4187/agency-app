from django.contrib import admin
from apps.products import models

# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    list_display = ['Product', 'Vendor', 'Price', 'GST']

admin.site.register(models.Product, ProductAdmin)