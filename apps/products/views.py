from django.shortcuts import render
from apps.agency.models import Agency
from apps.products.models import Product


# Create your views here.
def Products(request):
    active = Agency.objects.all().filter(Active=True)
    context = {
        'agency_obj' : Agency.objects.all(),
        'active_agency_obj' : active,
        'product_obj' : Product.objects.all(),
    }
    return render(request, 'products/product.html', {'obj':context})