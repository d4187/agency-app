from django.db import models
from django.conf import settings
from apps.agency.models import Agency
from apps.vendors.models import Vendor
# Create your models here.

# Product models
class Product(models.Model):
    User = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    Vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    Product = models.CharField(max_length=100)
    Price = models.CharField(max_length=100)
    GST = models.CharField(max_length=100)
    
    def __str__(self) :
        return self.Product 