from django.core import validators
from django import forms
from django.db.models import fields
from django.db.models.fields.files import FileField
from django.forms import widgets
from apps.vendors import models
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Row, Column
from crispy_forms.bootstrap import FormActions
 

class VendorForm(forms.ModelForm):
    class Meta:
        model = models.Vendor
        fields = '__all__'
        widgets = {
            'User': forms.TextInput(attrs={'class':'form-control ','value':'', 'type':'hidden'}),
        } 

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        Row(
            Column('Vendor_Type', css_class="col-md-2 pe-0"),
            Column('Vendor_Name', css_class="col-md-3 pe-0"),
            Column('Company_Name', css_class="col-md-3 pe-0"),
            Column('Phone', css_class="col-md-2 "),
            Column('Email', css_class="col-md-2 ps-0"),
            Column('Address', css_class="col-md-4 pe-0 mt-2"),
            Column('City', css_class="col-md-2 pe-0 mt-2"),
            Column('Zip', css_class="col-md-1 pe-0 mt-2"),
            Column('State', css_class="col-md-3 pe-0 mt-2"),
            Column('Country', css_class="col-md-2  mt-2"),
            Column('Agency', css_class="col-md-4  mt-2"),
            Column('GST_No', css_class="col-md-4 px-0 mt-2"),
            Column('Vendor_Staff_Detail', css_class="col-md-4 mt-2"),
            Column('Origin_Of_The_Material', css_class="col-md-4 mt-2"),
            Column('No_Of_Trucks', css_class="col-md-4 px-0 mt-2"),
            Column('Material_Supplying', css_class="col-md-4 mt-2"),
            Column('User', css_class="col-md-12 mt-2"),
            css_class='form-row row'
        ),
        FormActions(
            Submit('add_vendor', 'Add Vendor', css_class="mt-2  btn-sm"),
            css_class='form-row add_agencies'
        )
    )


class VendorStaffDetailForm(forms.ModelForm):
    class Meta:
        model = models.VendorStaffDetail
        fields = '__all__'
        widgets = {
            'Address': forms.Textarea(attrs={'class':'form-control ', 'rows':'1'}),
            'Payment_Address': forms.Textarea(attrs={'class':'form-control ', 'rows':'1'}),
            'Driver_Details': forms.Textarea(attrs={'class':'form-control ', 'rows':'1'}),
        }
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        Row(
            Column('Vendor_Company_Name', css_class="col-md-3 mb-1 "),
            Column('Account_Name', css_class="col-md-3 px-0"),
            Column('Mobile', css_class="col-md-3 pe-0"),
            Column('Mail_Id', css_class="col-md-3 "),
            Column('Address', css_class="col-md-12"),
            Column('Payment_Name', css_class="col-md-4 mt-2 "),
            Column('Payment_Mobile', css_class="col-md-4 mt-2 px-0"),
            Column('Payment_Mail_Id', css_class="col-md-4 mt-2 "),
            Column('Payment_Address', css_class="col-md-6 mt-2 pe-2"),
            Column('Driver_Details', css_class="col-md-6 mt-2 ps-2"),
            Column(
                FormActions(
                    Submit('add_vendorstaffdetail', 'Add Staff Detail', css_class="mt-2  btn-sm")
                )
            ),
            css_class='form-row row'
        ),  
    )
    
    # def clean(self) :
    #     return super().clean()


class VendorGSTForm(forms.ModelForm):

    class Meta:
        model = models.VendorGST
        fields = '__all__'

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        Row(
            Column('Vendor_Company_Name', css_class="col-md-3 pe-0"),
            Column('Vendor_GST_Type', css_class="col-md-3 pe-0"),
            Column('Vendor_GST_No', css_class="col-md-3 pe-0"),
            Column('Vendor_PAN_No', css_class="col-md-3 "),
            Column('Vendor_Bank_Account', css_class="col-md-6 pe-2 my-2"),
            Column('Vendor_Certificate', css_class="col-md-6 ps-2 my-2"),
            Column('Vendor_Phone_Number', css_class="col-md-4 "),
            Column('Vendor_Email', css_class="col-md-4 px-0"),

            Column('Vendor_Tin', css_class="col-md-4"),

            Column(
                FormActions(
                    Submit('add_vendorgst', 'Add Vendor GST', css_class="mt-2  btn-sm")
                )
            ),
            css_class='form-row row'
        ),
    )


class VendorCertificateForm(forms.ModelForm):

    class Meta:
        model = models.VendorCertificate
        fields = '__all__'
        widgets = {
            'Vendor_Certificate_Scan_Copy': forms.FileInput(attrs={'class':'form-control ', 'required': False, }),
        }

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        Row(
            Column('Vendor_Company_Name', css_class="col-md-6 pe-2"),
            Column('Vendor_Certificate_Name', css_class="col-md-6 ps-2"),
            Column('Vendor_Certificate_Scan_Copy', css_class="col-md-12 ps-2 mt-2"),
            Column(
                FormActions(
                    Submit('add_vendorcertificate', 'Add Vendor Certificate', css_class="mt-0 btn-sm")
                )
            ),
            css_class='form-row row'
        ),
    )


class VendorGSTBankAccountForm(forms.ModelForm):

    class Meta:
        model = models.VendorGSTBankAccount
        fields = '__all__'

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        Row(
            Column('Vendor_Company_Name', css_class="col-md-6 pe-2"),
            Column('Address', css_class="col-md-6 ps-2"),
            Column('City', css_class="col-md-5 mt-2 pe-2"),
            Column('Zip', css_class="col-md-2 px-2 mt-2"),
            Column('State', css_class="col-md-5  mt-2"),
            Column(
                FormActions(
                    Submit('add_vendorgstbankaccount', 'Add Vendor GST Bank Account', css_class="mt-2 btn-sm")
                )
            ),
            css_class='form-row row'
        ),
    )

