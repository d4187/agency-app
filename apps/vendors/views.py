from django.shortcuts import render, redirect
from apps.vendors.models import Vendor
from apps.agency.models import Agency
from apps.vendors import forms
from django.contrib import messages
from apps.home.models import Notification
# Create your views here.

def Vendors(request):
    active = Agency.objects.all().filter(Active=True)
    context = {
        'agency_obj' : Agency.objects.all(),
        'active_agency_obj' : active,
        'vendor_obj' : Vendor.objects.all().filter(Agency=active[0].id),
    }

    return render(request, 'vendors/vendors.html', {'obj':context})
 

def Add_Vendor(request):

    vf = forms.VendorForm(request.POST or None)
    vsf = forms.VendorStaffDetailForm(request.POST or None)
    vgf = forms.VendorGSTForm(request.POST, request.FILES or None)
    vcf = forms.VendorCertificateForm(request.POST, request.FILES or None)
    vgbf = forms.VendorGSTBankAccountForm(request.POST or None)

    active = Agency.objects.all().filter(Active=True)
    context = {
        'agency_obj' : Agency.objects.all(),
        'active_agency_obj' : active,
        'vendor_form': vf,
        'vendor_staff_form': vsf,
        'vendor_gst_form': vgf,
        'vendor_certificate_form': vcf,
        'vendor_gstbank_form': vgbf,
    }

    if request.method == 'POST':
        getColumn = list(request.POST.keys())
        getvalues = list(request.POST.values())


        if getColumn[2] == 'Address':
            if request.POST['Address'] != '':
                obj = vgbf.save()
                UpdateNotification(obj.id, 'vendors', 'A', request.user)
                messages.success(request, 'Bank Account Added. !!!')
                return redirect('/vendors/add-vendor/')
            else:
                messages.error(request, 'Address not found. !!!') 

        elif getColumn[2] == 'Vendor_Certificate_Name':
            if request.POST['Vendor_Certificate_Name'] != '':
                obj = vcf.save()
                UpdateNotification(obj.id, 'vendors', 'A', request.user)
                messages.success(request, 'Vendor Certificate  Added. !!!')
                return redirect('/vendors/add-vendor/')
            else:
                messages.error(request, 'Vendor certificate not found. !!!') 

        elif getColumn[2] == 'Vendor_GST_Type':
            if request.POST['Vendor_GST_Type'] != '':
                obj = vgf.save()
                UpdateNotification(obj.id, 'vendors', 'A', request.user)
                messages.success(request, ' Vendor GST Detail Added. !!!')
                return redirect('/vendors/add-vendor/')
            else:
                messages.error(request, 'Vendor gst detail not found. !!!') 

        
        elif getColumn[2] == 'Account_Name':
            if request.POST['Account_Name'] != '':
                obj = vsf.save()
                UpdateNotification(obj.id, 'vendors', 'A', request.user)                
                messages.success(request, ' Vendor Staff Detail Added. !!!')
                return redirect('/vendors/add-vendor/')
            else:
                messages.error(request, 'Vendor staff detail not found. !!!')     

        elif getColumn[2] == 'Vendor_Name':

            if request.POST['Vendor_Name'] != '':
                obj = vf.save()
                UpdateNotification(obj.id, 'vendors', 'A', request.user)                
                messages.success(request, ' Vendor Detail Added. !!!')
                return redirect('/vendors/add-vendor/')
            else:
                messages.error(request, 'Vendor detail not found. !!!') 

                

    return render(request, 'vendors/vendor_add.html', {'obj':context})


def Update_Vendor(request, pk=None):

    vendor_data =  Vendor.objects.get(id=pk)
    vf = forms.VendorForm(request.POST or request.FILES or None, instance=vendor_data)
    
    active = Agency.objects.all().filter(Active=True)
    context = {
        'agency_obj' : Agency.objects.all(),
        'active_agency_obj' : active,
        'vendor_form': vf,
    }

    if request.method == 'POST':

        if request.POST['Vendor_Name'] != '':
            obj = vf.save()
            UpdateNotification(obj.id, 'vendors', 'E', request.user)   
            messages.success(request, 'Vendor Updated. !!!')
        else:
            messages.error(request, 'Vendor Name is empty !!!')

        return render(request, 'vendors/update-vendor.html', {'obj':context})

    return render(request, 'vendors/update-vendor.html', {'obj':context})



def UpdateNotification(id, modal, type, user):

    if user.is_staff:
        Noti = {
            "id":id,
            "model":modal
        }
        obj = Notification.objects.create(Notifications=str(Noti), Types=type, User=user, Status=False)

    return obj