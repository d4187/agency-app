from django.contrib import admin
from apps.vendors import models

# Register your models here.
class VendorAdmin(admin.ModelAdmin):
    list_display = ['Vendor_Type', 'Vendor_Name', 'Company_Name', 'Phone', 'Email', 'Origin_Of_The_Material', 'No_Of_Trucks']

admin.site.register(models.Vendor, VendorAdmin)


# Register your models here.
class VendorStaffDetailAdmin(admin.ModelAdmin):
    list_display = ['Vendor_Company_Name', 'Account_Name', 'Payment_Name', 'Driver_Details']

admin.site.register(models.VendorStaffDetail, VendorStaffDetailAdmin)


# Register your models here.
class VendorGSTAdmin(admin.ModelAdmin):
    list_display = ['Vendor_Company_Name', 'Vendor_GST_Type', 'Vendor_GST_No', 'Vendor_PAN_No', 'Vendor_Phone_Number', 'Vendor_Email']

admin.site.register(models.VendorGST, VendorGSTAdmin)


# Register your models here.
class VendorGSTBankAccountAdmin(admin.ModelAdmin):
    list_display = ['Address', 'City', 'State', 'Zip']

admin.site.register(models.VendorGSTBankAccount, VendorGSTBankAccountAdmin)


# # Register your models here.
class VendorCertificateAdmin(admin.ModelAdmin):
    list_display = ['Vendor_Certificate_Name', 'Vendor_Certificate_Scan_Copy']

admin.site.register(models.VendorCertificate, VendorCertificateAdmin)