from django.db import models
from django.conf import settings
from apps.agency.models import Agency
from django.utils import timezone
# Create your models here.

GST_TYPE_CHOICES =(
    ("0", "GST User"),
    ("1", "Unregistered User"),
)

VENDOR_CATEGORY_CHOICES =(
    ("Q", "Quarry Owner"),
    ("T", "Transport Owner"),
    ("C", "Crusher Owner"),
)



""" Vendor GST BANK """
class VendorGSTBankAccount(models.Model):
    Vendor_Company_Name = models.CharField(max_length=100, blank=True, null=True)
    Address = models.CharField(max_length=50,null=True, blank=True)
    City = models.CharField(max_length=50,null=True, blank=True)
    State = models.CharField(max_length=50,null=True, blank=True)
    Zip = models.CharField(max_length=50,null=True, blank=True)

    def __str__(self):
        return '{0} - {1}'.format(self.Vendor_Company_Name, self.Address)


""" Vendor Certificate """
class VendorCertificate(models.Model):
    Vendor_Company_Name = models.CharField(max_length=100, blank=True, null=True)
    Vendor_Certificate_Name = models.CharField(max_length=100, blank=True, null=True)
    Vendor_Certificate_Scan_Copy = models.FileField(upload_to ='uploads/%Y/%m/%d/', blank=True, null=True)

    def __str__(self) :
        return '{0} - {1}'.format(self.Vendor_Company_Name, self.Vendor_Certificate_Name)


"""Vendor GST"""
class VendorGST(models.Model):
    Vendor_Company_Name = models.CharField(max_length=100, blank=True, null=True)
    Vendor_GST_Type = models.CharField(max_length=1, choices=GST_TYPE_CHOICES, default=1, blank=True, null=True)
    Vendor_GST_No = models.CharField(max_length=50, blank=True, null=True)
    Vendor_PAN_No = models.CharField(max_length=50, blank=True, null=True)
    Vendor_Certificate = models.ManyToManyField(VendorCertificate)
    Vendor_Tin = models.CharField(max_length=50, blank=True, null=True)
    Vendor_Bank_Account = models.ManyToManyField(VendorGSTBankAccount)
    Vendor_Phone_Number = models.CharField(max_length=50, blank=True, null=True)
    Vendor_Email = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self) :
        return '{0} - {1}'.format(self.Vendor_Company_Name, self.Vendor_GST_No)



"""Vendor Staff Detail"""
class VendorStaffDetail(models.Model):
    Vendor_Company_Name = models.CharField(max_length=50, blank=True, null=True)
    Account_Name = models.CharField(max_length=50, blank=True, null=True)
    Mobile = models.CharField(max_length=50, blank=True, null=True)
    Mail_Id = models.CharField(max_length=50, blank=True, null=True)
    Address = models.TextField(blank=True, null=True)
    Payment_Name = models.CharField(max_length=50, blank=True, null=True)
    Payment_Mobile = models.CharField(max_length=50, blank=True, null=True)
    Payment_Mail_Id = models.CharField(max_length=50, blank=True, null=True)
    Payment_Address = models.TextField(blank=True, null=True)
    Driver_Details = models.TextField(blank=True, null=True)
    Published = models.DateTimeField(auto_now_add=True)

    def __str__(self) :
        return '{0} - {1}'.format(self.Vendor_Company_Name, self.Account_Name)


""" Venor models"""
class Vendor(models.Model):
    User = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    Agency = models.ManyToManyField(Agency)
    Vendor_Type = models.CharField(max_length=1, choices=GST_TYPE_CHOICES, default=1, blank=True, null=True)
    Vendor_Name = models.CharField(max_length=100, blank=True, null=True)
    Company_Name = models.CharField(max_length=100, blank=True, null=True)
    Phone = models.CharField(max_length=100, blank=True, null=True)
    Email = models.CharField(max_length=100, blank=True, null=True)
    Address = models.CharField(max_length=100, blank=True, null=True)
    City = models.CharField(max_length=100, blank=True, null=True)
    State = models.CharField(max_length=100, blank=True, null=True)
    Zip = models.CharField(max_length=100, blank=True, null=True)
    Country = models.CharField(max_length=100, blank=True, null=True)
    GST_No = models.ManyToManyField(VendorGST)
    Origin_Of_The_Material = models.CharField(max_length=100, blank=True, null=True)
    No_Of_Trucks = models.CharField(max_length=100, blank=True, null=True)
    Material_Supplying = models.CharField(max_length=100, blank=True, null=True)
    Vendor_Staff_Detail = models.ManyToManyField(VendorStaffDetail)
    published = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.Vendor_Name

