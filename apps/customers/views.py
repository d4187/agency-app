from django.shortcuts import render, redirect
from apps.customers.models import Customer
from apps.agency.models import Agency
from django.contrib import messages
from apps.customers import forms
# Create your views here.


def Customers(request):
    active = Agency.objects.all().filter(Active=True)
    context = {
        'agency_obj' : Agency.objects.all(),
        'active_agency_obj' : active,
        'customer_obj' : Customer.objects.filter(),
    }
    return render(request, 'customers/customer.html', {'obj':context})


def Add_Customer(request):

    cf = forms.CustomerForm(request.POST, request.FILES or None)
    csf = forms.CustomerStaffForm(request.POST or None)
    cdaf = forms.CustomerDeliveryAddressForm(request.POST or None)
    cbaf = forms.CustomerGSTBillingAddressForm(request.POST, request.FILES or None)
    cacf = forms.CustomerBankAccountsForm(request.POST or None)
    
    active = Agency.objects.all().filter(Active=True)
    context = {
        'agency_obj' : Agency.objects.all(),
        'active_agency_obj' : active,
        'customer_form': cf,
        'customer_staff_form': csf,
        'customer_delivery_address_form': cdaf,
        'customer_billing_address_form': cbaf,
        'customer_bank_accounts_form': cacf,
    }

    if request.method == 'POST':
        getColumn = list(request.POST.keys())


        if getColumn[2] == 'Category':
            if request.POST['Category'] != '':
                obj = cacf.save()
                messages.success(request, 'Customer Bank Account Added. !!!')
                return redirect('/customers/add-customer')
            else:
                messages.error(request, 'Address not found. !!!') 

        elif getColumn[1] == 'GST_No':
            if request.POST['GST_No'] != '':
                obj = cbaf.save()
                messages.success(request, 'Customer GST No Added. !!!')
                return redirect('/customers/add-customer')
            else:
                messages.error(request, 'Customer GST No not found. !!!') 

        elif getColumn[2] == 'Branch':
            if request.POST['Branch'] != '':
                obj = cdaf.save()
                messages.success(request, 'Customer Delivery Address Added. !!!')
                return redirect('/customers/add-customer')
            else:
                messages.error(request, 'Customer delivery address not found. !!!') 

        
        elif getColumn[1] == 'Name':
            if request.POST['Name'] != '':
                obj = csf.save()
                messages.success(request, 'Customer Staff Detail Added. !!!')
                return redirect('/customers/add-customer')
            else:
                messages.error(request, 'Customer staff detail not found. !!!')     
 
        elif getColumn[1] == 'Founder_Name':

            if request.POST['Founder_Name'] != '':
                obj = cf.save()
                messages.success(request, 'Customer Detail Added. !!!')
                return redirect('/customers/add-customer')
            else:
                messages.error(request, 'Founder Name detail not found. !!!') 

                

    return render(request, 'customers/customer-add.html', {'obj':context})


def Update_Customer(request, pk=None):
    customer_data = Customer.objects.get(id=pk)
    cm = forms.CustomerForm(request.POST or request.FILES or None, instance=customer_data)
    active = Agency.objects.all().filter(Active=True)
    context = {
        'cm' : cm,
        'agency_obj' : Agency.objects.all(),
        'active_agency_obj' : active,
    }

    if request.method == 'POST':

        if request.POST['Founder_Name'] != '':
            obj = cm.save()
            messages.success(request, 'Customer Updated. !!!')
        else:
            messages.error(request, 'Customer Name is empty !!!')

    return render(request, 'customers/customer-update.html', {'obj':context})