from django.db import models
from django.conf import settings
from django.utils import timezone

# Create your models here.

CATEGORY_CHOICES =(
    ("0", "Personal"),
    ("1", "Registered Account"),
)


""" Customer bank account models"""
class CustomerBankAccounts(models.Model):
    Customer_Name = models.CharField(max_length=100, null=True, blank=True)
    Category = models.CharField(max_length=1, choices=CATEGORY_CHOICES, null=True, blank=True)
    Account_No = models.CharField(max_length=100, null=True, blank=True)
    IFSC_Code = models.CharField(max_length=100, null=True, blank=True)
    Branch = models.CharField(max_length=100, null=True, blank=True)
    Register_Phone = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return '{0} - {1}'.format(self.Customer_Name, self.Branch)


""" Customer GST Billing Address """
class CustomerGSTBillingAddress(models.Model):
    GST_No = models.CharField(max_length=100, null=True, blank=True)
    Scan_Copy = models.FileField(upload_to ='uploads/%Y/%m/%d/', blank=True, null=True)
    Address = models.CharField(max_length=100, null=True, blank=True)
    City = models.CharField(max_length=20, null=True, blank=True)
    State = models.CharField(max_length=30, null=True, blank=True)
    Zip = models.CharField(max_length=10, null=True, blank=True)
    Country = models.CharField(max_length=30, null=True, blank=True)
    Location_Url = models.URLField(max_length=100, null=True, blank=True)
    Mobile = models.CharField(max_length=20, null=True, blank=True)
    Mail_Id = models.CharField(max_length=50, null=True, blank=True)
    Photo = models.FileField(upload_to ='uploads/%Y/%m/%d/', blank=True, null=True)

    def __str__(self):
        return '{0} - {1}'.format(self.GST_No, self.City)


""" Delivery Address Models """
class CustomerDeliveryAddress(models.Model):
    Customer_Name = models.CharField(max_length=200, null=True, blank=True)
    Branch = models.CharField(max_length=200, null=True, blank=True)
    URL_Location_Of_The_Unloading_Place = models.URLField(max_length=200, null=True, blank=True)
    URL_Office_Location  = models.URLField(max_length=200, null=True, blank=True)
    URL_Of_Owner_House  = models.URLField(max_length=200, null=True, blank=True)
    Invoice_Submitting_URL = models.URLField(max_length=200, null=True, blank=True)
    Payment_Collecting_URL = models.URLField(max_length=200, null=True, blank=True)
    Add_other_URL = models.URLField(max_length=200, null=True, blank=True)

    def __str__(self):
        return '{0} - {1}'.format(self.Customer_Name, self.Branch)


POSITION_CHOICES = (
    ("PH", "Purchase Head"),
    ("AH", "Account Head"),
    ("QH", "QC Head"),
    ("PU", "Payment Head"),
    ("US", "Unloading Suprivisor"),
    ("SP", "Security"),
)

GST_TYPE = (
    ("0", "GST Registered"),
    ("1", "Unregistered")
)
 
""" Customer Address """
class CustomerStaff(models.Model):
    Name = models.CharField(max_length=100, null=True, blank=True) 
    Contact_No = models.CharField(max_length=20, null=True, blank=True)
    Email = models.EmailField(max_length=30, null=True, blank=True)
    Position = models.CharField(max_length=2, choices=POSITION_CHOICES) 
    Location = models.CharField(max_length=100, null=True, blank=True) 
    City = models.CharField(max_length=50, null=True, blank=True)
    Zip = models.CharField(max_length=50, null=True, blank=True)
    State = models.CharField(max_length=50, null=True, blank=True) 
    Country = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return '{0} - {1}'.format(self.Name, self.Position)


"""delivery address for sales company should be only one"""
class Customer(models.Model):
    # map customer name
    User = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    Founder_Name = models.CharField(max_length=100, null=True, blank=True)
    Company_Name = models.CharField(max_length=100, null=True, blank=True)
    Founder_Phone = models.CharField(max_length=50, null=True, blank=True)
    Founder_Email = models.CharField(max_length=50, null=True, blank=True)
    Founder_Address = models.TextField(null=True, blank=True)
    Customer_Staff_Account = models.ManyToManyField(CustomerStaff)
    Bank_Account = models.ManyToManyField(CustomerBankAccounts)
    Delivery_Address = models.ManyToManyField(CustomerDeliveryAddress)
    GST_Type = models.CharField(max_length=1, choices=GST_TYPE)
    GST_No = models.ForeignKey(CustomerGSTBillingAddress, on_delete=models.CASCADE, null=True, blank=True)
    PAN_No = models.CharField(max_length=100, null=True, blank=True)
    PAN_Scan_Copy = models.FileField(upload_to ='uploads/%Y/%m/%d/', blank=True, null=True)
    No_Of_Project = models.CharField(max_length=10, null=True, blank=True)
    Remarks = models.TextField(null=True, blank=True)
    Payment_Terms = models.CharField(max_length=100, null=True, blank=True)
    published = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.Founder_Name




















"""
po number

fields -> (po applicable or not applicable), 
if applicable 
    ponumber
    poquantity
    porate
    podate
else
    salesrate
    order recieved date
"""

"""
bankaccount
=================
account no, ifsc code, branch, register phone

"""

"""
agencies

old
==========
name, email, phone, logo, 
new
============
gstno , pan no, certification, tin (optional), bankaccount(as foreign), phone number, mailid

"""


"""
customer detail add 
=========================
agency
user
customer name
material name, offer price, quantity,noofloadsload, expected date, quality,  
for every materiaal need to add moisiture
"""

"""
moisture
if appicable (%)
else
dont put %
"""


""" remarks (notes)"""









"""
material
=============

product - select
price





"""