from django.core import validators
from django import forms
from django.db.models import fields
from django.db.models.fields.files import FileField
from django.forms import widgets
from apps.agency import models
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Row, Column
from crispy_forms.bootstrap import FormActions
 
class AgencyForm(forms.ModelForm):
    class Meta:
        model = models.Agency
        fields = '__all__'
        widgets = {
            'Logo': forms.FileInput(attrs={'class':'form-control ', 'required': False, }),
             'User': forms.TextInput(attrs={'class':'form-control ','value':'', 'type':'hidden'}),
        }

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        Row(
            Column('Agency_Name', css_class="col-md-2 pe-0"),
            Column('Email', css_class="col-md-2 pe-0"),
            Column('Phone', css_class="col-md-2 pe-0"),
            Column('GST_Number', css_class="col-md-2 pe-0"),
            Column('PAN_Number', css_class="col-md-2 pe-0"),
            Column('Tin', css_class="col-md-2 "),
            Column('User', css_class="col-md-12 pe-0"),
            Column(
                'Logo', css_class="col-md-4 pe-0 mt-2"
                ),
            Column('Bank_Account', css_class="col-md-4 pe-0 mt-2"),
            Column('Certificates', css_class="col-md-4 mt-2"),
            css_class='form-row row'
        ),
        FormActions(
            Submit('add_agency', 'Add Agency', css_class="mt-0  btn-sm"),
            css_class='form-row add_agencies'
        )
    )


class AgencyBankAccount(forms.ModelForm):
    class Meta:
        model = models.AgencyBankAccount
        fields = '__all__'

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        Row(
            Column('Account_Number', css_class="col-md-2 mb-1 pe-0"),
            Column('IFSC_Code', css_class="col-md-2 pe-0"),            
            Column('Bank', css_class="col-md-2 pe-0"),
            Column('Branch', css_class="col-md-2 pe-0"),
            Column('Register_Email', css_class="col-md-2 pe-0"),
            Column('Register_Phone', css_class="col-md-2 "),
            Column(
                FormActions(
                    Submit('add_bankaccount', 'Add Bank Account', css_class="mt-2 btn-sm")
                )
            ),
            css_class='form-row row'
        ),  
    )
    
    # def clean(self) :
    #     return super().clean()

class AgencyCertificate(forms.ModelForm):

    class Meta:
        model = models.AgencyCertificate
        fields = '__all__'
        widgets = {
            'Certificate_Scan_Copy': forms.FileInput(attrs={'class':'form-control '}),
        }

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        Row(
            Column('Certificate_Name', css_class="col-md-5 pe-0"),
            Column('Certificate_Scan_Copy', css_class="col-md-5 pe-0"),
            Column(
                FormActions(
                    Submit('add_certifcate', 'Add Certificate', css_class="mt-4  btn-sm")
                )
            ),
            css_class='form-row row'
        ),
    )

