from django.shortcuts import render, redirect
from django.utils.functional import empty
from apps.agency import models
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
# specific to this view
from django.views.generic import ListView, CreateView, TemplateView
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.urls import reverse_lazy
from apps.agency import forms
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib import messages
from apps.home.models import Notification

from apps.home.models import Notification
# Create your views here.


def Agencies(request):
    context = {
        'agency_obj' : models.Agency.objects.all(),
        'active_agency_obj' : models.Agency.objects.all().filter(Active=True),
    }
    return render(request, 'agencies/agency.html', {'obj':context})


def Add_Agency(request):

    fm_ba = forms.AgencyBankAccount(request.POST or None)
    fm_cer = forms.AgencyCertificate(request.POST, request.FILES or None)
    fm = forms.AgencyForm(request.POST, request.FILES or None)

    context = {
        'fm_ba': fm_ba,
        'fm_cer': fm_cer,
        'fm' : fm,
        'agency_obj' : models.Agency.objects.all(),
        'active_agency_obj' : models.Agency.objects.all().filter(Active=True),
    }

    if request.method == 'POST':

        # get columns as keys
        getColumn = list(request.POST.keys())
        # check account submit
        if getColumn[1] == 'Account_Number':
            # check data already exists
            if not models.AgencyBankAccount.objects.filter(Account_Number=request.POST['Account_Number']) and request.POST['Account_Number'] != '':
                obj = fm_ba.save()
                UpdateNotification(obj.id, 'agency', 'A', request.user)
                messages.success(request, 'Bank Account Added. !!!')
                return redirect('/agencies/add-agency')
            else:
                messages.error(request, 'Account Number not found or already exists. !!!')     

        # check certification
        elif getColumn[1] == 'Certificate_Name':
            if not models.AgencyCertificate.objects.filter(Certificate_Name=request.POST['Certificate_Name']) and request.POST['Certificate_Name'] != '':
                obj = fm_cer.save()
                UpdateNotification(obj.id, 'agency', 'A', request.user)
                messages.success(request, 'Certificate Added. !!!')
                return redirect('/agencies/add-agency')
            else:
                messages.error(request, 'Certificate not found or already exists. !!!')     

        else:
            if not models.Agency.objects.filter(Agency_Name=request.POST['Agency_Name']) and request.POST['Agency_Name'] != '':
                obj = fm.save()
                UpdateNotification(obj.id, 'agency', 'A', request.user)
                messages.success(request, 'Agency Added. !!!')
                return redirect('/agencies/add-agency')
            else:
                messages.error(request, 'Agency Name is empty or already exists. !!!') 

        return render(request, 'agencies/add-agency.html', {'obj':context})

    else:

        return render(request, 'agencies/add-agency.html', {'obj':context})



def Update_Agency(request, pk=None):

    agency_data =  models.Agency.objects.get(id=pk)
    fm = forms.AgencyForm(request.POST or request.FILES or None, instance=agency_data)

    context = {
        'fm' : fm,
        'agency_obj' : models.Agency.objects.all(),
        'active_agency_obj' : models.Agency.objects.all().filter(Active=True),
    }

    if request.method == 'POST':

        if request.POST['Agency_Name'] != '':
            obj = fm.save()
            messages.success(request, 'Agency Updated. !!!')
        else:
            messages.error(request, 'Agency Name is empty !!!')

        return render(request, 'agencies/update-agency.html', {'obj':context})

    return render(request, 'agencies/update-agency.html', {'obj':context})



def updateagency(request, pk=None):
    allagency = models.Agency.objects.filter(User=request.user.id).update(Active=False)
    updatebyidagency = models.Agency.objects.filter(id=pk).update(Active=True)
    return JsonResponse({'status':200, 'message':'Agency updated. !!!'})


def UpdateNotification(id, modal, type, user):

    if user.is_staff:
        Noti = {
            "id":id,
            "model":modal
        }
        obj = Notification.objects.create(Notifications=str(Noti), Types=type, User=user, Status=False)

    return obj
