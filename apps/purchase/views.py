from django.shortcuts import render
from apps.orders.models import Order
# Create your views here.


def Purchases(request):
    return render(request, 'purchase/purchase.html')

def PurchasesInvoice(request):
    return render(request, 'invoice/purchase-invoice.html')

def PurchasesVoucher(request):
    return render(request, 'voucher/purchase-voucher.html')

def PurchasesVoucherAdd(request):
    return render(request, 'voucher/purchase-voucher-add.html')

def AddPurchase(request):
    context = {
        'order': Order.objects.filter()
    }
    return render(request, 'purchase/purchase-add.html', {'context': context})

def AddInvoices(request):
    context = {
        'order': Order.objects.filter()
    }
    return render(request, 'invoice/add-purchase-invoice.html', {'context': context})
