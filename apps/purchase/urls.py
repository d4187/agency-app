from django.urls import path
from apps.purchase import views
 
urlpatterns = [
    path('', views.Purchases, name="purchase"),
    path('purchase-invoice', views.PurchasesInvoice, name="purchase-invoice"),
    path('purchase-voucher', views.PurchasesVoucher, name="purchase-voucher"),
    path('purchase-voucher-add', views.PurchasesVoucherAdd, name="purchase-voucher-add"),
    path('add-purchase', views.AddPurchase, name="add-purchase"),
    path('add-invoice', views.AddInvoices, name="add-invoice"),
] 