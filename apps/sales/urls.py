from django.urls import path
from apps.sales import views

urlpatterns = [
    path('', views.sales, name='sales'),
    path('sales-voucher', views.SalesVoucher, name="salesvoucher"),
    path('sales-voucher-add', views.SalesVoucherAdd, name="sales-voucher-add"),
    path('sales-invoice', views.SalesInvoice, name="sales-invoice"),
    path('sales-invoice-add', views.SalesInvoiceAdd, name="sales-invoice-add"),

]