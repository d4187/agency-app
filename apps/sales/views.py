from django.shortcuts import render

# Create your views here.
def sales(request):
    return render(request, 'sales/sales.html')

def SalesVoucher(request):
    return render(request, 'voucher/sales-voucher.html')

def SalesVoucherAdd(request):
    return render(request, 'voucher/sales-voucher-add.html')

def SalesInvoice(request):
    return render(request, 'invoice/sales-invoice.html')

def SalesInvoiceAdd(request):
    return render(request, 'invoice/add-sales-invoice.html')
    