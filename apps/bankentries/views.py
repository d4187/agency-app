from django.shortcuts import render

# Create your views here.
def BankEntries(request):
    return render(request, 'bank-entries/bank-entries.html')