from django.urls import path
from apps.bankentries import views

urlpatterns = [
    path('', views.BankEntries, name='bank_entries'),
]