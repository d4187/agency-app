from django.shortcuts import render

# Create your views here.
def Invoice(request):
    return render(request, 'invoice/invoice.html')