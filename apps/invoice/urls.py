from django.urls import path
from apps.invoice import views

urlpatterns = [
    path('', views.Invoice, name="invoice"),
]
