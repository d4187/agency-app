# Generated by Django 3.2.8 on 2021-12-06 11:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_alter_materialorder_mat_po_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='materialorder',
            old_name='Sale_Price',
            new_name='Fianl_PO_Price',
        ),
        migrations.AddField(
            model_name='materialorder',
            name='Final_Offer_Price',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
