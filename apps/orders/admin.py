from django.contrib import admin
from apps.orders.models import MaterialOrder, Order, PONumber, POMaterialInfo
# Register your models here.


class MaterialOrderAdmin(admin.ModelAdmin):
    list_display = ['Vendor', 'Material_Name', 'Offer_Price', 'No_Of_Load', 'Expected_Date', 'Quantity']

admin.site.register(MaterialOrder, MaterialOrderAdmin)


class POMaterialInfoAdmin(admin.ModelAdmin):
    list_display = ['PO_Material_Name', 'PO_Quantity', 'PO_Rates', 'PO_Quality', 'PO_Terms', 'Published']

admin.site.register(POMaterialInfo, POMaterialInfoAdmin)


class PONumberAdmin(admin.ModelAdmin):
    list_display = ['PO_Type', 'PO_Number', 'PO_Recived_From_Name', 'PO_Recived_From_Mail', 'PO_Date']

admin.site.register(PONumber, PONumberAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ['Agency', 'Sales_Company', 'PO_Number', 'Expected_Delivery_Date', 'Remarks']

admin.site.register(Order, OrderAdmin)