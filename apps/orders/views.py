from django.shortcuts import render
from apps.customers.models import Customer
from apps.agency.models import Agency
from apps.orders.models import Order
from apps.products.models import Product
from apps.orders import forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
# Create your views here.

def Orders(request):
    active_agency = Agency.objects.all().filter(Active=True)

    # PAGINATION
    if 'orders' in request.GET:
        # SEARCH
        ordersearch = request.GET.get('orders')
        lookups = Q(Sales_Company__Founder_Name__icontains=ordersearch) | Q(Sales_Company__Company_Name__icontains=ordersearch) | Q(PO_Number__PO_Number__icontains=ordersearch)
        Order_list = Order.objects.filter(lookups)
    else:
        Order_list = Order.objects.all()
    paginator = Paginator(Order_list, 2)
    page = request.GET.get('page', 1)
    orders = paginator.page(page)

    context = {
        'agency_obj' : Agency.objects.all(),
        'order_obj' : Order.objects.all().filter(Agency=active_agency[0].id),
        'orders' : orders,
    }
    return render(request, 'orders/orders.html', {'obj':context})
 

def AddOrder(request):
    active_agency =Agency.objects.all().filter(Active=True)
    context = {
        'agency_obj' : Agency.objects.all(),
        'active_agency_obj' : active_agency,
        'product_obj' : Product.objects.values_list('Product', flat=True).distinct(),
        'customer_obj' : Customer.objects.all(),
        'mf': forms.MaterialOrderForm(request.POST or None)
    }
    return render(request, 'orders/add-order.html', {'obj':context})