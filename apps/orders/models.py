from django.db import models
from django.conf import settings
from apps.agency.models import Agency
from apps.vendors.models import Vendor
from apps.products.models import Product
from apps.customers.models import Customer, CustomerDeliveryAddress
# Create your models here.

StatusUnloading = [
    ('I', 'Initiator'),
    ('O', 'On Hold'),
    ('D', 'Delivered')
]


PO_TYPES = (
    ('PA', 'PO Applicable'),
    ('NA', 'Not Applicable')
)

MATERIAL_TYPE = (
    ('N', 'Natural'),
    ('M', 'Manufactured')
)

MOISTURE_TYPE = (
    ('', 'Yes'),
    ('0', 'No')
)

"""PO Material Info"""
class POMaterialInfo(models.Model):
    PO_Material_Name = models.CharField(max_length=100, blank=True, null=True)
    PO_Quantity = models.CharField(max_length=100, blank=True, null=True)
    PO_Rates = models.CharField(max_length=100, blank=True, null=True)
    PO_Quality = models.CharField(max_length=100, blank=True, null=True)
    PO_Terms = models.CharField(max_length=100, blank=True, null=True)
    Published = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{0}'.format(self.PO_Material_Name)


""" Po number Models"""
class PONumber(models.Model):
    PO_Type = models.CharField(choices=PO_TYPES, max_length=2) 
    PO_Number = models.CharField(max_length=100, blank=True, null=True) 
    Scan_Copy = models.FileField(upload_to ='uploads/%Y/%m/%d/', blank=True, null=True)
    PO_Recived_From_Name = models.CharField(max_length=50, blank=True, null=True)
    PO_Recived_From_Mail = models.CharField(max_length=50, blank=True, null=True)
    PO_Approved_By = models.CharField(max_length=50, blank=True, null=True)
    PO_Material_Info = models.ManyToManyField(POMaterialInfo)
    PO_Date = models.DateField(blank=True, null=True)
    Order_Recieved_Date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.PO_Number


""" Material order list """
class MaterialOrder(models.Model):
    User = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    Vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    Vendor_Type = models.CharField(max_length=100, blank=True, null=True)
    Location_Supply = models.CharField(max_length=100, blank=True, null=True)
    Payment_Capability = models.CharField(max_length=100, blank=True, null=True)
    Mat_PO_ID = models.ForeignKey(POMaterialInfo, on_delete=models.CASCADE)
    Product_Name = models.ForeignKey(Product, on_delete=models.CASCADE)
    Material_Name = models.CharField(max_length=100, blank=True, null=True)
    Offer_Price = models.CharField(max_length=50, blank=True, null=True)
    Fianl_PO_Price = models.CharField(max_length=50, blank=True, null=True)
    Final_Offer_Price = models.CharField(max_length=50, blank=True, null=True)
    No_Of_Load = models.CharField(max_length=50, blank=True, null=True)
    Expected_Date = models.DateField()
    Quantity = models.CharField(max_length=50, blank=True, null=True)
    CDC = models.CharField(max_length=50, blank=True, null=True)
    Vehicle_No = models.CharField(max_length=50, blank=True, null=True)
    HSNCode = models.CharField(max_length=50, blank=True, null=True)
    Quality_Material_Type = models.CharField(max_length=1, choices=MATERIAL_TYPE, default=1, blank=True, null=True)
    Moisture = models.CharField(max_length=1, choices=MOISTURE_TYPE, default=1, blank=True, null=True)
    Moisture_Number = models.CharField(max_length=50, blank=True, null=True)
    Material_Term = models.CharField(max_length=100, blank=True, null=True)
    Others = models.TextField()

    def __str__(self):
       return '{0}'.format(self.Material_Name)


""" Daily Order Info """
class Order(models.Model):
    User = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    Agency = models.ForeignKey(Agency, on_delete=models.CASCADE)
    Sales_Company = models.ForeignKey(Customer, on_delete=models.CASCADE)
    Sales_Delivery_Address = models.ManyToManyField(CustomerDeliveryAddress)	
    Billing_Address = models.TextField()		
    PO_Number = models.ForeignKey(PONumber, on_delete=models.CASCADE)
    Expected_Delivery_Date = models.DateField()
    Materials = models.ManyToManyField(MaterialOrder)
    Remarks = models.TextField()
    Approved = models.BooleanField(default=False)

    def __str__(self):
       return '{0}'.format(self.Sales_Company)

