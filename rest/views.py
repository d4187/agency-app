from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from rest import serializers
from apps.agency.models import Agency
from apps.vendors.models import Vendor
from apps.products.models import Product
from apps.customers.models import Customer, CustomerDeliveryAddress
from apps.orders.models import MaterialOrder, Order, PONumber, POMaterialInfo
from apps.home.models import Notification, Attendance
from apps.quicknotes.models import Quicknotes
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.pagination import PageNumberPagination


class AgencyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Agency.objects.all().order_by('-id')
    serializer_class = serializers.AgencySerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['Active']

class VendorsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Vendor.objects.all().order_by('-id')
    serializer_class = serializers.VendorSerializer
    permission_classes = [permissions.IsAuthenticated]
    pagination_class = serializers.Result_20_Pagination


class ProductsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Product.objects.all().order_by('-id')
    serializer_class = serializers.ProductSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['Product']


class ProductWithOutViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Product.objects.all().order_by('-id')
    serializer_class = serializers.ProductSerializerWithOutInfo
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['Product']


class CustomerDeliverAddressViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = CustomerDeliveryAddress.objects.all().order_by('-id')
    serializer_class = serializers.CustomerDeliveryAddressSerializer
    permission_classes = [permissions.IsAuthenticated]

    

class CustomersViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Customer.objects.all().order_by('-id')
    serializer_class = serializers.CustomerSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['Company_Name']
    pagination_class = serializers.Result_20_Pagination


class OrderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Order.objects.all().order_by('-id')
    serializer_class = serializers.OrderSerializer
    permission_classes = [permissions.IsAuthenticated]


class OrderWithOutViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Order.objects.all().order_by('-id')
    serializer_class = serializers.OrderWithOutSerializer
    permission_classes = [permissions.IsAuthenticated]


class MaterialInfoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = MaterialOrder.objects.all().order_by('-id')
    serializer_class = serializers.MaterialInfoSerializer
    permission_classes = [permissions.IsAuthenticated]


class MaterialInfoWithInfoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = MaterialOrder.objects.all().order_by('-id')
    serializer_class = serializers.MaterialInfoSerializerWithInfo
    permission_classes = [permissions.IsAuthenticated]


class POMaterialInfoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = POMaterialInfo.objects.all().order_by('-id')
    serializer_class = serializers.POMaterialInfoSerializer
    permission_classes = [permissions.IsAuthenticated]


class POViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = PONumber.objects.all().order_by('-id')
    serializer_class = serializers.PONumberSerializer
    permission_classes = [permissions.IsAuthenticated]


"""Notification start here"""
class NotificationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Notification.objects.all().order_by('-id')
    serializer_class = serializers.NotificationSerializer
    permission_classes = [permissions.IsAuthenticated]
    

"""Notification start here"""
class NotificationWithOutInfoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Notification.objects.all().order_by('-id')
    serializer_class = serializers.NotificationWithOutSerializer
    permission_classes = [permissions.IsAuthenticated]



"""Attendacne start here"""
class AttendanceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Attendance.objects.all().order_by('-id')
    serializer_class = serializers.AttendanceSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['Punch_Out_Time']


"""Wuick Notes start here"""
class QuickNotesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Quicknotes.objects.all().order_by('-id')
    serializer_class = serializers.QuickNotesSerializer
    permission_classes = [permissions.IsAuthenticated]


