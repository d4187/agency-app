from django.urls import path, include
from rest import views
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'products', views.ProductWithOutViewSet)
router.register(r'material-info', views.MaterialInfoViewSet)
router.register(r'orders', views.OrderWithOutViewSet)
router.register(r'notifications', views.NotificationWithOutInfoViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
