from django.urls import path, include
from rest import views
from rest_framework import routers

 
router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)
router.register(r'agency', views.AgencyViewSet)
router.register(r'notifications', views.NotificationViewSet)
router.register(r'attendances', views.AttendanceViewSet)
router.register(r'vendors', views.VendorsViewSet)
router.register(r'products', views.ProductsViewSet)
router.register(r'customers', views.CustomersViewSet)
router.register(r'customer-delivery-address', views.CustomerDeliverAddressViewSet)
router.register(r'po-material-info', views.POMaterialInfoViewSet)
router.register(r'po-number', views.POViewSet)
router.register(r'material-info', views.MaterialInfoWithInfoViewSet)
router.register(r'orders', views.OrderViewSet)
router.register(r'po-number', views.POViewSet)
router.register(r'quicknotes', views.QuickNotesViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
