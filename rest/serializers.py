from django.contrib.auth.models import User, Group
from django.utils.regex_helper import Choice
from rest_framework import serializers
from apps.agency.models import Agency, AgencyBankAccount, AgencyCertificate
from apps.vendors.models import Vendor, VendorGST, VendorStaffDetail
from apps.products.models import Product
from apps.customers.models import Customer, CustomerDeliveryAddress, CustomerStaff, CustomerGSTBillingAddress, CustomerBankAccounts, POSITION_CHOICES
from apps.orders.models import  MaterialOrder, Order, PONumber, POMaterialInfo
from apps.home.models import Notification, Attendance, TYPE_CHOICES
from apps.quicknotes.models import Quicknotes
from rest_framework.pagination import PageNumberPagination

# from django.contrib.auth import get_user_model


class ChoiceField(serializers.ChoiceField):

    def to_representation(self, obj):
        if obj == '' and self.allow_blank:
            return obj
        return self._choices[obj]

    def to_internal_value(self, data):
        # To support inserts with the value
        if data == '' and self.allow_blank:
            return ''

        for key, val in self._choices.items():
            if val == data:
                return key
        self.fail('invalid_choice', input=data)


class Result_20_Pagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 20

# user serializers
class UserSerializere(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email']


# SiteInfo,
class AgencyBankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgencyBankAccount
        fields = '__all__'


class AgencyCertificateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgencyCertificate
        fields = '__all__'


class AgencySerializer(serializers.ModelSerializer):
    Bank_Account = AgencyBankAccountSerializer(many=True)
    Certificates = AgencyCertificateSerializer(many=True)
    class Meta:
        model = Agency
        fields = '__all__'


class VendorGSTSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendorGST
        fields = '__all__'


class VendorStaffDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendorStaffDetail
        fields = '__all__'


class VendorSerializer(serializers.ModelSerializer):
    GST_No = VendorGSTSerializer(many=True)
    Vendor_Staff_Detail = VendorStaffDetailSerializer(many=True)
    class Meta:
        model = Vendor
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    Vendor = VendorSerializer()
    class Meta:
        model = Product
        fields = '__all__'

    def create(self, validated_data):
        print(validated_data)
        choices = validated_data.pop('Vendor')
        vendor = Vendor.objects.create(**validated_data)
        return vendor


class ProductSerializerWithOutInfo(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'




class CustomerDeliveryAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerDeliveryAddress
        fields = '__all__'


class CustomerStaffSerializer(serializers.ModelSerializer):
    Position = ChoiceField(choices=POSITION_CHOICES)
    class Meta:
        model = CustomerStaff
        fields = '__all__'


class CustomerGSTBillingSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerGSTBillingAddress
        fields = '__all__'


class CustomerBankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerBankAccounts
        fields = '__all__'


class CustomerSerializer(serializers.ModelSerializer):
    GST_No = CustomerGSTBillingSerializer()
    Delivery_Address = CustomerDeliveryAddressSerializer(many=True)
    Customer_Staff_Account = CustomerStaffSerializer(many=True)
    Bank_Account = CustomerBankAccountSerializer(many=True)

    class Meta:
        model = Customer
        fields = '__all__'


class POMaterialInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = POMaterialInfo
        fields = '__all__'


class MaterialInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaterialOrder
        fields = '__all__'


class MaterialInfoSerializerWithInfo(serializers.ModelSerializer):
    Vendor = VendorSerializer()
    Product_Name = ProductSerializerWithOutInfo()
    Mat_PO_ID = POMaterialInfoSerializer()
    class Meta:
        model = MaterialOrder
        fields = '__all__'





class PONumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = PONumber
        fields = '__all__'




class OrderSerializer(serializers.ModelSerializer):
    PO_Number = PONumberSerializer()
    Sales_Company = CustomerSerializer()
    Sales_Delivery_Address = CustomerDeliveryAddressSerializer(many=True)
    Agency = AgencySerializer()
    Materials = MaterialInfoSerializer(many=True)
    class Meta:
        model = Order
        fields = '__all__'


class OrderWithOutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


# Notification start Here
class NotificationSerializer(serializers.ModelSerializer):
    Types = ChoiceField(choices=TYPE_CHOICES)
    User = UserSerializere()
    class Meta:
        model = Notification
        fields = '__all__'


# Notification start Here
class NotificationWithOutSerializer(serializers.ModelSerializer):
    Types = ChoiceField(choices=TYPE_CHOICES)
    class Meta:
        model = Notification
        fields = '__all__'
        
""" Attendance start here """
class AttendanceSerializer(serializers.ModelSerializer):
    User = UserSerializere()
    class Meta:
        model = Attendance
        fields = '__all__'

""" Quick Notes Start Here """
class QuickNotesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quicknotes
        fields = '__all__'    